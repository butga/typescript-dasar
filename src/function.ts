//tipe data pada balikan function

function getName():string {
    return "Hello, my name is butga";
}

console.log(getName());

function getAge():number {
    return 12;
}

//kalau mau make method yg tanpa balikan
function printName():void {
    console.log("print name");
}

printName();

//tipedata dalam argumen
//berfungsi untuk menjaga data parameter sesuai dengan yang diinginkan

function multiply(val1: number, val2:number): number {
    return val1 * val2;
}

const result = multiply(3, 2);
console.log(result);

//function as type

type Tambah = (val1: number, val2: number) => number;

const Add: Tambah = (val1: number, val2: number): number => {
    return val1 + val2;
}

//default parameter
const fullName = (first: string, last: string = "fajar"): string => {
    return first + " " + last;
}

console.log(fullName("butga"));

//optional parameter
const getUmur = (val1: string, val2?: string): string => {
    return val1 + " " + val2;
}

console.log(getUmur("A"));

