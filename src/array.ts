//array
let array: number[];
array = [1, 2, 3];

let array2: string[];
array2 = ["String 1", "String 2"];

let array3: any[];
array3 = ["String", 1, true];

//tuples
//semacam array tapi tipe datanya terbatas
let biodata: [string, number];
biodata = ["Surabaya", 123];
