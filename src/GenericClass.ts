class List<T> {
    private data: T[];

    //rest parameter pakai ...
    constructor(...elements: T[]) {
        this.data = elements;
    }

    add(element: T): void {
        this.data.push(element);
    }

    addMultiple(...element: T[]): void {
        this.data.push(...element);
    }

    getAll(): T[] {
        return this.data;
    }
}

//kalau inputan number
let numbers = new List<number>(1, 2, 3);
numbers.add(4);
numbers.addMultiple(5, 6, 7);
console.log(numbers.getAll());


//kalau mau bebas
let random = new List<number | string>(1, "a", 2);
console.log(random.getAll());



