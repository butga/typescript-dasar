"use strict";
//tipe data pada balikan function
function getName() {
    return "Hello, my name is butga";
}
console.log(getName());
function getAge() {
    return 12;
}
//kalau mau make method yg tanpa balikan
function printName() {
    console.log("print name");
}
printName();
//tipedata dalam argumen
//berfungsi untuk menjaga data parameter sesuai dengan yang diinginkan
function multiply(val1, val2) {
    return val1 * val2;
}
var result = multiply(3, 2);
console.log(result);
var Add = function (val1, val2) {
    return val1 + val2;
};
//default parameter
var fullName = function (first, last) {
    if (last === void 0) { last = "fajar"; }
    return first + " " + last;
};
console.log(fullName("butga"));
//optional parameter
var getUmur = function (val1, val2) {
    return val1 + " " + val2;
};
console.log(getUmur("A"));
