"use strict";
var List = /** @class */ (function () {
    //rest parameter pakai ...
    function List() {
        var elements = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            elements[_i] = arguments[_i];
        }
        this.data = elements;
    }
    List.prototype.add = function (element) {
        this.data.push(element);
    };
    List.prototype.addMultiple = function () {
        var _a;
        var element = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            element[_i] = arguments[_i];
        }
        (_a = this.data).push.apply(_a, element);
    };
    List.prototype.getAll = function () {
        return this.data;
    };
    return List;
}());
//kalau inputan number
var numbers = new List(1, 2, 3);
numbers.add(4);
numbers.addMultiple(5, 6, 7);
console.log(numbers.getAll());
//kalau mau bebas
var random = new List(1, "a", 2);
console.log(random.getAll());
